# 接口信息统一返回格式和Controller参数校验Starter包

## 介绍
*注意:* 本 starter 仅限于 spring boot 项目的使用

## 如何使用
1. 在配置文件中定义 2 个配置
- zcj.common.web.response-format.enable=true或false(true代表启用，false代表不启用)
- zcj.common.web.response-format.prefix=自定义值(例如：ATH等值
- 配置完成后，在 Controller 方法或类上定义注解 ResponseFormat 代表启用统一返回格式，而 IgnoreResponseFormat 代表不启用统一返回格式

2. Controller 方法参数校验功能（上述配置必须配置为 true 才可以进行启用）
- 例如：@NotBlank(message = "PWD_BLANK@密码为空")，以 @ 为分隔符，前半个为 subCode 码，与 prefix 结合，为ATH.PWD_BLANK，subMsg就为密码为空