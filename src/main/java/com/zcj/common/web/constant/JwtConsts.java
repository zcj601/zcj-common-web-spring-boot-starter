package com.zcj.common.web.constant;

/**
 * @classname JwtConsts
 * @description Jwt相关常量
 * @author: zchun
 * @version: 1.0
 * @date: 2021/9/5 15:09
 */
public class JwtConsts {

    /* 默认head */
    public static final String DEFAULT_HEADER = "{\"alg\": \"HS256\",\"typ\": \"JWT\"}";

    /* HmacSHA256 加密算法 秘钥 */
    public static final String SECRET = "12345";

    /* token有效时间  1天 */
    public static final long EXPIRE_TIME = 1000 * 60 * 60 * 24;

    /* token在header中的名字 */
    public static final String HEADER_TOKEN_NAME = "Authorization";

    /**
     * JWT令牌前缀
     */
    public static final String AUTHORIZATION_PREFIX = "bearer ";

    /**
     * Basic认证前缀
     */
    public static final String BASIC_PREFIX = "Basic ";

    /**
     * JWT载体key
     */
    public static final String JWT_PAYLOAD_KEY = "payload";

    /**
     * JWT存储权限前缀
     */
    public static final String AUTHORITY_PREFIX = "ROLE_";

    /**
     * JWT存储权限属性
     */
    public static final String JWT_AUTHORITIES_KEY = "authorities";
}
