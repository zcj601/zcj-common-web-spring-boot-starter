package com.zcj.common.web.constant;

/**
 * @classname CustomSubCodeEnum
 * @description 自定义业务二级Code常量池类(顶级Code值为 ： 40004)
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 22:38
 */
public enum CustomSubCodeEnum {

    SYSTEM_EXCEPTION("SYSTEM_EXCEPTION", "系统错误"),
    PARAM_ILLEGAL("PARAM_ILLEGAL", "请求参数异常"),
    OPERATE_FAILED("OPERATE_FAILED", "操作失败"),
    RETURN_DATA_EMPTY("RETURN_DATA_EMPTY", "返回数据为空"),
    USERNAME_NOT_FOUND("USERNAME_NOT_FOUND", "用户不存在"),
    USERNAME_OR_PWD_ILLEGAL("USERNAME_OR_PWD_ILLEGAL", "用户名或密码异常"),
    ACCOUNT_ILLEGAL("ACCOUNT_ILLEGAL", "账户异常(禁用、锁定、过期)");

    /**
     * 二级返回码
     */
    private String code;

    /**
     * 响应码说明
     */
    private String msg;

    CustomSubCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
