package com.zcj.common.web.constant;

/**
 * @classname ResponseSubCodeEnum
 * @description 二级响应码枚举类
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 18:07
 */
public enum ResponseSubCodeEnum {

    /* 20000:服务不可用 */
    ISP_UNKNOW_ERROR("isp.unknow-error", "服务暂不可用（业务系统不可用）"),
    AOP_UNKNOW_ERROR("aop.unknow-error", "服务暂不可用（网关自身的未知错误）"),

    /* 20001:授权权限不足 */
    AOP_INVALID_AUTH_TOKEN("aop.invalid-auth-token", "无效的访问令牌"),
    AOP_AUTH_TOKEN_TIME_OUT("aop.auth-token-time-out", "访问令牌已过期"),
    CLIENT_AUTHENTICATION_FAILED("aop.client-authentication-failed", "客户端认证失败"),

    /* 40001:缺少必选参数 */
    ISV_MISSING_METHOD("isv.missing-method", "缺少方法名参数"),

    /* 40002:非法参数 */
    ISV_INVALID_PARAMETER("isv.invalid-parameter", "参数无效"),
    ISV_UPLOAD_FAIL("isv.upload-fail", "文件上传失败"),
    ISV_INVALID_FILE_EXTENSION("isv.invalid-file-extension", "文件扩展名无效"),
    ISV_INVALID_FILE_SIZE("isv.invalid-file-size", "文件大小无效"),
    ISV_INVALID_METHOD("isv.invalid-method", "不存在的方法名"),

    /* 40006:权限不足 */
    ISV_INSUFFICIENT_USER_PERMISSIONS("isv.insufficient-user-permissions", "用户权限不足"),

    /* 服务内部异常 */
    INNER_UNKNOW_ERROR("inner.unknow-error", "服务内部异常");

    /**
     * 二级返回码
     */
    private String code;

    /**
     * 二级返回码描述
     */
    private String msg;

    ResponseSubCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }}
