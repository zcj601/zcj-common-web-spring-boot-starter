package com.zcj.common.web.constant;

/**
 * @classname CloudConsts
 * @description 常量池
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/29 16:33
 */
public class CloudConsts {

    /**
     * 添加的请求头的头部
     */
    public static final String GATEWAY_TOKEN_HEADER = "gateway-kt";

    /**
     * 添加的请求头的值
     */
    public static final String GATEWAY_TOKEN_VALUE = "hello world";
}
