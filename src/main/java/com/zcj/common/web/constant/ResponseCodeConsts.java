package com.zcj.common.web.constant;

/**
 * @classname ResponseCodeConsts
 * @description 顶级code常量池类
 * @author: zchun
 * @version: 1.0
 * @date: 2021/7/3 21:07
 */
public class ResponseCodeConsts {

    /**
     * 接口调用成功
     */
    public static final int SUCCESS = 10000;
    /**
     * 服务不可用
     */
    public static final int SERVICE_UNAVAILABLE = 20000;
    /**
     * 授权的权限不足
     */
    public static final int INSUFFICIENT_AUTHORIZATION = 20001;
    /**
     * 缺少必选参数
     */
    public static final int MISSING_MANDATORY_PARAMETER = 40001;
    /**
     * 非法的参数
     */
    public static final int INVALID_PARAMETER = 40002;
    /**
     * 业务处理失败
     */
    public static final int BUSINESS_PROCESSING_FAILED = 40004;
    /**
     * 权限不足
     */
    public static final int INSUFFICIENT_AUTHORITY = 40006;

    /**
     * 未知错误
     */
    public static final int UNKNOW_ERROR = 500;
}
