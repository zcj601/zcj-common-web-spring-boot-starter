package com.zcj.common.web.constant;

/**
 * @classname CommonConSts
 * @description 通用常量池
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/9 9:27
 */
public class CommonConsts {

    /**
     * 字符串:点
     */
    public static final String STRING_DOT = ".";
    /**
     * 字符串:@
     */
    public static final String STRING_AT = "@";
    /**
     * 字符串:冒号
     */
    public static final String STRING_COLON = ":";
    /**
     * 字符串:底层横杠
     */
    public static final String STRING_BOTTOM_HORIZONTAL_BAR = "_";
}
