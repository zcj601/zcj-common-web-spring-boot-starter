package com.zcj.common.web.constant;

/**
 * @classname ResponseCodeEnum
 * @description 一级响应码枚举类
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 17:57
 */
public enum ResponseCodeEnum {

    SUCCESS(ResponseCodeConsts.SUCCESS, "成功"),
    SERVICE_UNAVAILABLE(ResponseCodeConsts.SERVICE_UNAVAILABLE, "服务不可用"),
    INSUFFICIENT_AUTHORIZATION(ResponseCodeConsts.INSUFFICIENT_AUTHORIZATION, "授权权限不足"),
    MISSING_REQUIRED_PARAMETER(ResponseCodeConsts.MISSING_MANDATORY_PARAMETER, "缺少必选参数"),
    ILLEGAL_PARAMETER(ResponseCodeConsts.INVALID_PARAMETER, "非法的参数"),
    BUSINESS_PROCESSING_FAILED(ResponseCodeConsts.BUSINESS_PROCESSING_FAILED, "业务处理失败"),
    UNKNOW_ERROR(ResponseCodeConsts.UNKNOW_ERROR, "未知异常");

    /**
     * 一级响应码
     */
    private int code;

    /**
     * 一级响应码说明
     */
    private String msg;

    ResponseCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }}
