package com.zcj.common.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zcj.common.web.constant.CustomSubCodeEnum;
import com.zcj.common.web.constant.ResponseCodeEnum;
import com.zcj.common.web.constant.ResponseSubCodeEnum;
import com.zcj.common.web.util.SubCodeUtils;

/**
 * @classname ResponseDTO
 * @description 统一对象格式（将返回对象数据封装至数据中）
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 17:38
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResponseDTO<T> extends BaseDTO {

    /**
     * 返回码
     */
    private Integer code;

    /**
     * 返回码描述
     */
    private String msg;

    /**
     * 明细返回码
     */
    private String subCode;

    /**
     * 明细返回码描述
     */
    private String subMsg;

    /**
     * 具体返回数据
     */
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSubMsg() {
        return subMsg;
    }

    public void setSubMsg(String subMsg) {
        this.subMsg = subMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void topCodeEnum(ResponseCodeEnum codeEnum) {
        if (codeEnum != null) {
            this.code = codeEnum.getCode();
            this.msg = codeEnum.getMsg();
        }
    }

    public void subCodeEnum(ResponseSubCodeEnum codeEnum, String prefix) {
        if (codeEnum != null) {
            this.subCode = SubCodeUtils.getFullSubCode(prefix, codeEnum.getCode());
            this.subMsg = codeEnum.getMsg();
        }
    }

    public void subCodeEnum(ResponseSubCodeEnum codeEnum) {
        subCodeEnum(codeEnum, null);
    }

    public void subCodeEnum(CustomSubCodeEnum codeEnum, String prefix) {
        if (codeEnum != null) {
            this.subCode = SubCodeUtils.getFullSubCode(prefix, codeEnum.getCode());
            this.subMsg = codeEnum.getMsg();
        }
    }

    public void subCodeEnum(CustomSubCodeEnum codeEnum) {
        subCodeEnum(codeEnum, null);
    }
}
