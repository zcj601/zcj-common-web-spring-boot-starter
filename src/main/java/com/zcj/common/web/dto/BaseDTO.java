package com.zcj.common.web.dto;

import java.io.Serializable;

/**
 * @classname BaseDTO
 * @description DTO域中的基类对象
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 17:44
 */
public class BaseDTO implements Serializable {
}
