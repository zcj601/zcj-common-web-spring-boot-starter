package com.zcj.common.web.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zcj.common.web.constant.ResponseSubCodeEnum;
import com.zcj.common.web.exception.ServiceUnavailableException;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @classname WebUtils
 * @description web 相关工具类
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/29 16:53
 */
public class WebUtils {

    /**
     * Response 流中写入json数据
     *
     * @param response
     * @param object
     */
    public static void writeJson(HttpServletResponse response, Object object) {

        // 必须含有数据
        if (response != null && object != null) {
            PrintWriter out = null;
            try {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(new ObjectMapper().writeValueAsString(object));
            } catch (Exception e) {
                throw new ServiceUnavailableException(ResponseSubCodeEnum.AOP_UNKNOW_ERROR);
            } finally {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }
        }
    }
}
