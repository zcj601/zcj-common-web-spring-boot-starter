package com.zcj.common.web.util;

import org.springframework.http.HttpHeaders;

import java.util.*;

/**
 * @classname CustomFeignUtils
 * @description feign 的相关工具类
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/21 23:09
 */
public class CustomFeignUtils {

    /**
     * 将 headers 对象转换成集合对象
     *
     * @param httpHeaders 头部对象
     * @return 集合对象
     */
    public static Map<String, Collection<String>> getHeaders(HttpHeaders httpHeaders) {
        LinkedHashMap<String, Collection<String>> headers = new LinkedHashMap();
        Iterator iterator = httpHeaders.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, List<String>> entry = (Map.Entry) iterator.next();
            headers.put(entry.getKey(), entry.getValue());
        }

        return headers;
    }

    /**
     * 获取请求头信息
     *
     * @param headers 头部信息
     * @return 请求头
     */
    public static HttpHeaders getHttpHeaders(Map<String, Collection<String>> headers) {
        HttpHeaders httpHeaders = new HttpHeaders();
        Iterator var2 = headers.entrySet().iterator();

        while (var2.hasNext()) {
            Map.Entry<String, Collection<String>> entry = (Map.Entry) var2.next();
            httpHeaders.put((String) entry.getKey(), new ArrayList((Collection) entry.getValue()));
        }

        return httpHeaders;
    }
}
