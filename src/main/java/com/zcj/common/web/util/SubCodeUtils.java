package com.zcj.common.web.util;

import com.zcj.common.web.constant.CommonConsts;
import org.apache.commons.lang3.StringUtils;

/**
 * @classname SubCodeUtils
 * @description 二级响应码工具类
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/9 9:17
 */
public class SubCodeUtils {

    /**
     * 组装二级返回码
     * 注：若前缀为空，则直接返回 subCode
     *
     * @param prefix  前缀
     * @param subCode 二级响应码
     * @return 组装完成的二级响应码
     */
    public static String getFullSubCode(String prefix, String subCode) {
        // 若 subCode 为空，直接返回null
        if (StringUtils.isBlank(subCode)) {
            return null;
        }

        // 若前缀为空，则直接返回二级响应码
        if (StringUtils.isBlank(prefix)) {
            return subCode;
        }

        // 拼接字符串，以点进行连接
        return prefix + CommonConsts.STRING_DOT + subCode;
    }

    /**
     * 获取完整二级响应码
     *
     * @param prefix    前缀
     * @param subCode   二级响应码
     * @param otherInfo 其余信息
     * @return 完整二级响应码
     */
    public static String getFullSubCode(String prefix, String subCode, String otherInfo) {
        // 先组装前两个字符串
        String info = getFullSubCode(prefix, subCode);

        if (StringUtils.isBlank(info)) {
            info = otherInfo;
        } else if (StringUtils.isNotBlank(otherInfo)) {
            info = info + CommonConsts.STRING_BOTTOM_HORIZONTAL_BAR + StringUtils.strip(otherInfo);
        }

        return info;
    }
}
