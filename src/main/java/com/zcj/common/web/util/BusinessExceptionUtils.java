package com.zcj.common.web.util;

import com.zcj.common.web.constant.CustomSubCodeEnum;
import com.zcj.common.web.exception.BusinessException;

/**
 * @classname BusinessExceptionUtils
 * @description 业务处理操作工具类
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/22 15:28
 */
public class BusinessExceptionUtils {

    /**
     * 根据code和msg构造对象
     *
     * @param code 二级明细码
     * @param msg  二级明细码说明
     * @return BusinessException 对象
     */
    public static BusinessException build(String code, String msg) {
        return new BusinessException(code, msg);
    }

    /**
     * 构造 BusinessException 对象
     *
     * @param customSubCodeEnum 二级码枚举
     * @return BusinessException 对象
     */
    public static BusinessException build(CustomSubCodeEnum customSubCodeEnum) {
        return new BusinessException(customSubCodeEnum);
    }
}
