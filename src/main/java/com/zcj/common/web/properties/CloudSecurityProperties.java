package com.zcj.common.web.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @classname CloudSecurityProperties
 * @description TODO
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/29 16:59
 */
@ConfigurationProperties(prefix = "zcj.common.web.cloud")
public class CloudSecurityProperties {

    /**
     * 是否只能通过网关获取资源
     * 默认为True
     */
    private Boolean onlyFetchByGateway = Boolean.TRUE;

    public Boolean getOnlyFetchByGateway() {
        return onlyFetchByGateway;
    }

    public void setOnlyFetchByGateway(Boolean onlyFetchByGateway) {
        this.onlyFetchByGateway = onlyFetchByGateway;
    }
}
