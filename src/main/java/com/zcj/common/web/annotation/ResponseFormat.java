package com.zcj.common.web.annotation;

import java.lang.annotation.*;

/**
 * @classname ResponseFormat
 * @description 统一格式返回注解
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 10:49
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ResponseFormat {

    /**
     * 若返回数据为空，是否显示错误
     *
     * @return true:显示错误，false:不错误
     */
    boolean isEmptyError() default false;
}
