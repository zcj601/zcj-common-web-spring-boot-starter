package com.zcj.common.web.annotation;

import java.lang.annotation.*;

/**
 * @classname IgnoreResponseFormat
 * @description 取消统一格式封装数据
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 10:50
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface IgnoreResponseFormat {
}
