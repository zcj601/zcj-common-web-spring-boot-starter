package com.zcj.common.web.handler;

import com.zcj.common.web.constant.CommonConsts;
import com.zcj.common.web.constant.CustomSubCodeEnum;
import com.zcj.common.web.constant.ResponseCodeEnum;
import com.zcj.common.web.constant.ResponseSubCodeEnum;
import com.zcj.common.web.dto.ResponseDTO;
import com.zcj.common.web.exception.*;
import com.zcj.common.web.util.SubCodeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;


/**
 * @classname ResponseExceptionHandler
 * @description 全局响应异常处理器
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 22:32
 */
@ConditionalOnWebApplication
@ConditionalOnProperty(value = "zcj.common.web.response-format.enable")
@RestControllerAdvice
public class ResponseExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(ResponseExceptionHandler.class);

    /**
     * 二级code的前缀码
     */
    @Value("${zcj.common.web.response-format.prefix}")
    private String subCodePrefix;

    /**
     * 处理自定义异常
     *
     * @param e 自定义异常
     * @return 自定义异常处理结果
     */
    @ExceptionHandler(value = ResponseException.class)
    public ResponseDTO handle(ResponseException e) {
        log.warn("自定义异常", e);
        return getResponseDTO(e);
    }

    /**
     * 处理Controller方法参数未通过校验异常
     *
     * @param e 参数校验异常
     * @return 响应对象
     */
    @ExceptionHandler(value = {ConstraintViolationException.class, MethodArgumentNotValidException.class, BindException.class, ValidationException.class})
    public ResponseDTO handleParameterException(Exception e) {

        // 错误信息
        String msg = null;

        if (e instanceof ConstraintViolationException) {
            msg = StringUtils.substringAfterLast(e.getMessage(), CommonConsts.STRING_COLON);
        } else if (e instanceof MethodArgumentNotValidException) {
            BindingResult bindingResult = ((MethodArgumentNotValidException) e).getBindingResult();
            // 获取第一个不合法的参数
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                msg = fieldError.getDefaultMessage();
            }
        } else if (e instanceof BindException) {
            // 获取第一个不合法的参数
            FieldError fieldError = ((BindException) e).getFieldError();
            if (fieldError != null) {
                msg = fieldError.getDefaultMessage();
            }
        } else {
            msg = e.getMessage();
        }

        // 判断信息是否为空
        if (StringUtils.isBlank(msg)) {
            msg = "处理参数异常";
        }

        // 完整二级响应码
        String subCode = SubCodeUtils.getFullSubCode(subCodePrefix, CustomSubCodeEnum.PARAM_ILLEGAL.getCode(), StringUtils.substringBeforeLast(msg, CommonConsts.STRING_AT));

        // 二级响应码明细信息
        String subMsg = StringUtils.substringAfterLast(msg, CommonConsts.STRING_AT);
        if (StringUtils.isBlank(subMsg)) {
            subMsg = CustomSubCodeEnum.PARAM_ILLEGAL.getMsg();
        }

        // 组装响应对象
        ResponseDTO dto = new ResponseDTO();
        dto.setCode(ResponseCodeEnum.BUSINESS_PROCESSING_FAILED.getCode());
        dto.setMsg(ResponseCodeEnum.BUSINESS_PROCESSING_FAILED.getMsg());
        dto.setSubCode(subCode);
        dto.setSubMsg(subMsg);

        return dto;
    }

    /**
     * 处理400异常
     *
     * @param e 400异常
     * @return 响应对象
     */
    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDTO handle400(Exception e) {
        return getResponseDTOHttpType(HttpStatus.BAD_REQUEST, e);
    }

    /**
     * 处理404异常
     *
     * @param e 404异常
     * @return 响应对象
     */
    @ExceptionHandler(value = {NoHandlerFoundException.class, HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseDTO handle404(Exception e) {
        return getResponseDTOHttpType(HttpStatus.NOT_FOUND, e);
    }

    /**
     * 处理其他异常
     *
     * @param e 异常
     * @return 响应对象
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseDTO handle500(Exception e) {
        log.error("内部错误", e);
        return getResponseDTOHttpType(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    //***************************私有工具类***********************************

    /**
     * 根据异常类型，组装成相应的对象
     *
     * @param e 自定义异常
     * @return 响应对象
     */
    private ResponseDTO getResponseDTO(ResponseException e) {
        if (e == null) {
            return null;
        }

        // 定义返回类型
        ResponseDTO dto = new ResponseDTO();
        dto.setSubCode(SubCodeUtils.getFullSubCode(subCodePrefix, e.getCode()));
        dto.setSubMsg(e.getMsg());

        // 根据异常类型组装相应的对象
        getResponseDTOByExceptionType(e, dto);

        return dto;
    }

    /**
     * 根据自定义异常类型，组装dto对象
     *
     * @param e   异常
     * @param dto 需要组装的对象
     */
    private void getResponseDTOByExceptionType(ResponseException e, ResponseDTO dto) {
        if (e instanceof BusinessException) { // 业务异常
            dto.setCode(ResponseCodeEnum.BUSINESS_PROCESSING_FAILED.getCode());
            dto.setMsg(ResponseCodeEnum.BUSINESS_PROCESSING_FAILED.getMsg());
        } else if (e instanceof ServiceUnavailableException) {   // 服务不可用
            dto.setCode(ResponseCodeEnum.SERVICE_UNAVAILABLE.getCode());
            dto.setMsg(ResponseCodeEnum.SERVICE_UNAVAILABLE.getMsg());
        } else if (e instanceof MissingMandatoryParameterException) {  // 缺少必选参数
            dto.setCode(ResponseCodeEnum.MISSING_REQUIRED_PARAMETER.getCode());
            dto.setMsg(ResponseCodeEnum.MISSING_REQUIRED_PARAMETER.getMsg());
        } else if (e instanceof InvalidParameterException) {   // 非法的参数
            dto.setCode(ResponseCodeEnum.ILLEGAL_PARAMETER.getCode());
            dto.setMsg(ResponseCodeEnum.ILLEGAL_PARAMETER.getMsg());
        } else if (e instanceof InsufficientAuthorityException) {  // 权限不足
            dto.setCode(ResponseCodeEnum.INSUFFICIENT_AUTHORIZATION.getCode());
            dto.setMsg(ResponseCodeEnum.INSUFFICIENT_AUTHORIZATION.getMsg());
        } else {    // 未知异常
            dto.setCode(ResponseCodeEnum.UNKNOW_ERROR.getCode());
            dto.setMsg(ResponseCodeEnum.UNKNOW_ERROR.getMsg());
        }
    }

    /**
     * 根据HTTP码错误，获取响应对象
     *
     * @param status HTTP状态
     * @param e      详细错误异常
     * @return 响应对象
     */
    private ResponseDTO getResponseDTOHttpType(HttpStatus status, Exception e) {
        ResponseDTO dto = new ResponseDTO();
        dto.setCode(status.value());
        dto.setMsg(status.getReasonPhrase());
        dto.setSubCode(ResponseSubCodeEnum.INNER_UNKNOW_ERROR.getCode());
        dto.setSubMsg(e.getMessage());

        return dto;
    }
}
