package com.zcj.common.web.handler;

import com.zcj.common.web.annotation.IgnoreResponseFormat;
import com.zcj.common.web.annotation.ResponseFormat;
import com.zcj.common.web.constant.CustomSubCodeEnum;
import com.zcj.common.web.constant.ResponseCodeEnum;
import com.zcj.common.web.dto.ResponseDTO;
import com.zcj.common.web.util.SubCodeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @classname CommonResponseHandler
 * @description 全局统一返回对象格式
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 22:32
 */
@ConditionalOnWebApplication
@ConditionalOnProperty(value = "zcj.common.web.response-format.enable")
@RestControllerAdvice
public class CommonResponseHandler implements ResponseBodyAdvice {

    /**
     * 二级code的前缀码
     */
    @Value("${zcj.common.web.response-format.prefix}")
    private String subCodePrefix;

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {

        //判断方法上是否能获取 ResponseFormat 注解
        if (methodParameter.getMethodAnnotation(ResponseFormat.class) == null
                && methodParameter.getDeclaringClass().getAnnotation(ResponseFormat.class) == null) {
            return false;
        }


        //判断方法中是否存在 IgnoreResponseFormat 注解，若存在，则返回false
        if (methodParameter.getMethodAnnotation(IgnoreResponseFormat.class) != null) {
            return false;
        }


        //判断类上是否存在 IgnoreResponseFormat 注解，若存在，则返回false
        if (methodParameter.getDeclaringClass().getAnnotation(IgnoreResponseFormat.class) != null) {
            return false;
        }
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest request, ServerHttpResponse response) {

        // 获取统一格式返回对象
        ResponseFormat responseFormat = methodParameter.getMethodAnnotation(ResponseFormat.class);
        if (responseFormat == null) {
            responseFormat = methodParameter.getDeclaringClass().getAnnotation(ResponseFormat.class);
        }

        if (responseFormat != null) {

            // 创建返回对象
            ResponseDTO dto = new ResponseDTO();

            //判断对象为空条件下是否转入Exception处理
            if (o == null && responseFormat.isEmptyError()) {
                // 返回错误对象
                dto.setCode(ResponseCodeEnum.BUSINESS_PROCESSING_FAILED.getCode());
                dto.setMsg(ResponseCodeEnum.BUSINESS_PROCESSING_FAILED.getMsg());
                dto.setSubCode(SubCodeUtils.getFullSubCode(subCodePrefix, CustomSubCodeEnum.RETURN_DATA_EMPTY.getCode()));
                dto.setSubMsg(CustomSubCodeEnum.RETURN_DATA_EMPTY.getMsg());
            } else {
                // 赋予成功对象
                dto.setCode(ResponseCodeEnum.SUCCESS.getCode());
                dto.setMsg(ResponseCodeEnum.SUCCESS.getMsg());
                dto.setData(o);
            }

            o = dto;

        }

        return o;
    }
}
