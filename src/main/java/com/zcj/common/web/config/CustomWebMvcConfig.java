package com.zcj.common.web.config;

import com.zcj.common.web.interceptor.ServerProtectInterceptor;
import com.zcj.common.web.properties.CloudSecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @classname CustomWebMvcConfig
 * @description 消除 StringHttpMessageConverter 转 String 错误配置
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/17 22:42
 */
@ConditionalOnWebApplication
@ConditionalOnProperty(value = "zcj.common.web.response-format.enable")
@EnableConfigurationProperties(CloudSecurityProperties.class)
@Configuration
public class CustomWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private CloudSecurityProperties properties;

    @Bean
    public HandlerInterceptor serverProtectInterceptor() {
        ServerProtectInterceptor interceptor = new ServerProtectInterceptor();
        interceptor.setProperties(properties);
        return interceptor;
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 删除 springboot 默认的 StringHttpMessageConverter 解析器
        converters.removeIf(x -> x instanceof StringHttpMessageConverter);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(serverProtectInterceptor());
    }
}
