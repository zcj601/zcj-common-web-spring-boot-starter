package com.zcj.common.web.config;

import org.hibernate.validator.HibernateValidator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * @classname ValidatorConfig
 * @description 参数校验，只要一个错误，后续不用校验，避免返回全部的信息
 * @author: zchun
 * @version: 1.0
 * @date: 2021/7/4 15:00
 */
@ConditionalOnWebApplication
@ConditionalOnProperty(value = "zcj.common.web.response-format.enable")
@Configuration
public class ValidatorConfig {

    @Bean
    public Validator validator() {
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .buildValidatorFactory();
        return validatorFactory.getValidator();
    }
}
