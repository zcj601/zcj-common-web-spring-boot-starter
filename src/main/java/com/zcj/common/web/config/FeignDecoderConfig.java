package com.zcj.common.web.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zcj.common.web.constant.ResponseCodeEnum;
import com.zcj.common.web.dto.ResponseDTO;
import com.zcj.common.web.util.CustomFeignUtils;
import feign.FeignException;
import feign.Response;
import feign.Util;
import feign.codec.DecodeException;
import feign.codec.Decoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpMessageConverterExtractor;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.nio.charset.Charset;

/**
 * @classname FeignDecoderConfig
 * @description feign的解码器（主要针对 ResponseDTO 获取 data 解码，而不是获取完整对象）
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/21 15:58
 */
@ConditionalOnWebApplication
@Component
public class FeignDecoderConfig implements Decoder {

    private static final Logger log = LoggerFactory.getLogger(FeignDecoderConfig.class);

    private ObjectMapper customObjectMapper = new ObjectMapper();

    @Autowired
    private ObjectFactory<HttpMessageConverters> messageConverters;

    @Override
    public Object decode(final Response response, Type type) throws IOException, DecodeException, FeignException {
        if (type instanceof Class || type instanceof ParameterizedType || type instanceof WildcardType) {
            @SuppressWarnings({"unchecked", "rawtypes"})
            HttpMessageConverterExtractor<?> extractor = new HttpMessageConverterExtractor(type, this.messageConverters.getObject().getConverters());
            return extractor.extractData(new FeignResponseAdapter(response));
        }

        System.err.println("type is not an instance of Class or ParameterizedType: " + type);
//        throw new DecodeException("type is not an instance of Class or ParameterizedType: " + type);
        return null;
    }

    private class FeignResponseAdapter implements ClientHttpResponse {

        private Response response;

        private FeignResponseAdapter(Response response) {
            this.response = response;
        }

        @Override
        public HttpStatus getStatusCode() throws IOException {
            return HttpStatus.valueOf(this.response.status());
        }

        @Override
        public int getRawStatusCode() throws IOException {
            return this.response.status();
        }

        @Override
        public String getStatusText() throws IOException {
            return this.response.reason();
        }

        @Override
        public void close() {
            try {
                this.response.body().close();
            } catch (IOException ex) {
                // Ignore exception on close...
            }
        }

        @Override
        public InputStream getBody() throws IOException {
            // 获取请求对象流
            InputStream inputStream = this.response.body().asInputStream();

            if (response.status() >= 200 && response.status() <= 299) {
                // 获取对象 Json 字符串
                String result = Util.toString(response.body().asReader(Charset.defaultCharset()));
                ResponseDTO dto = null;
                try {
                    // 读取对象
                    dto = customObjectMapper.readValue(result, ResponseDTO.class);
                } catch (JsonProcessingException e) {
                    log.warn("Feign 解析错误: ", e);
                }
                if (dto != null && dto.getCode() != null) {
                    byte[] bytes;

                    // 根据对象获取具体对象流
                    if (dto.getCode().intValue() == ResponseCodeEnum.SUCCESS.getCode() && dto.getData() != null) {
                        bytes = customObjectMapper.writeValueAsBytes(dto.getData());
                    } else {
                        bytes = new byte[0];
                    }

                    getNewResponse(bytes);
                    inputStream = this.response.body().asInputStream();
                }
            }

            return inputStream;
        }

        /**
         * 获取新的Response对象
         *
         * @param bytes body对象的数组
         */
        private void getNewResponse(byte[] bytes) {
            Response response = Response.builder()
                    .body(bytes)
                    .headers(this.response.headers())
                    .reason(this.response.reason())
                    .request(this.response.request())
                    .status(this.response.status())
                    .build();
            // 关闭当前流对象
            close();

            // 新建流对象
            this.response = response;
        }

        @Override
        public HttpHeaders getHeaders() {
            return CustomFeignUtils.getHttpHeaders(this.response.headers());
        }
    }
}
