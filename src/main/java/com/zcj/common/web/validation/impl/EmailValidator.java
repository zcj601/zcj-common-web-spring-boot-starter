package com.zcj.common.web.validation.impl;

import com.zcj.common.util.tool.util.EmailUtils;
import com.zcj.common.web.validation.annotation.EmailValidation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @classname EmailValidator
 * @description 完整邮箱名验证实现器
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/15 23:03
 */
public class EmailValidator implements ConstraintValidator<EmailValidation, String> {

    @Override
    public void initialize(EmailValidation constraintAnnotation) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        return EmailUtils.isValidEmail(email);
    }
}
