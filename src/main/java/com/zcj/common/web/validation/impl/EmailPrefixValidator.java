package com.zcj.common.web.validation.impl;

import com.zcj.common.util.tool.util.EmailUtils;
import com.zcj.common.web.validation.annotation.EmailPrefixValidation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @classname EmailPrefixValidator
 * @description 邮箱前缀验证
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/15 23:19
 */
public class EmailPrefixValidator implements ConstraintValidator<EmailPrefixValidation, String> {


    @Override
    public void initialize(EmailPrefixValidation constraintAnnotation) {

    }

    @Override
    public boolean isValid(String emailPrefix, ConstraintValidatorContext context) {
        return EmailUtils.isValidEmailPrefix(emailPrefix);
    }
}
