package com.zcj.common.web.validation.annotation;

import com.zcj.common.web.validation.impl.EmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @classname EmailValidation
 * @description 完整邮箱名验证
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/15 22:59
 */
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EmailValidator.class)
@Target(value = {ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface EmailValidation {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
