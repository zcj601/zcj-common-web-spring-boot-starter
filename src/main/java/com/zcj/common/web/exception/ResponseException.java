package com.zcj.common.web.exception;

/**
 * @classname ResponseException
 * @description 自定义响应异常
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/9 9:41
 */
public class ResponseException extends RuntimeException {

    /**
     * 二级响应码
     */
    protected String code;

    /**
     * 二级响应说明
     */
    protected String msg;

    public ResponseException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
