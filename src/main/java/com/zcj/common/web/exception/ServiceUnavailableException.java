package com.zcj.common.web.exception;

import com.zcj.common.web.constant.ResponseSubCodeEnum;

/**
 * @classname ServiceUnavailableException
 * @description 服务不可用异常（顶级code为：20000）
 * @author: zchun
 * @version: 1.0
 * @date: 2021/7/4 13:21
 */
public class ServiceUnavailableException extends ResponseException {

    public ServiceUnavailableException(String code, String msg) {
        super(code, msg);
    }

    public ServiceUnavailableException(ResponseSubCodeEnum subCodeEnum) {
        super(subCodeEnum != null ? subCodeEnum.getCode() : null, subCodeEnum != null ? subCodeEnum.getMsg() : null);
    }
}
