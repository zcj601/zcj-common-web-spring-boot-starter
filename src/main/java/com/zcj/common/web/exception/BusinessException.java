package com.zcj.common.web.exception;

import com.zcj.common.web.constant.CustomSubCodeEnum;

/**
 * @classname BusinessException
 * @description 业务处理异常
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/8 22:14
 */
public class BusinessException extends ResponseException {

    public BusinessException(String code, String msg) {
        super(code, msg);
    }

    public BusinessException(CustomSubCodeEnum codeEnum) {
        this(codeEnum != null ? codeEnum.getCode() : null, codeEnum != null ? codeEnum.getMsg() : null);
    }
}
