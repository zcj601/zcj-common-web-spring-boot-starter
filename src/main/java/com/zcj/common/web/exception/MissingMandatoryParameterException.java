package com.zcj.common.web.exception;

import com.zcj.common.web.constant.ResponseSubCodeEnum;

/**
 * @classname MissingMandatoryParameterException
 * @description 缺少必选参数异常（顶级code为：40001）
 * @author: zchun
 * @version: 1.0
 * @date: 2021/7/4 13:23
 */
public class MissingMandatoryParameterException extends ResponseException {

    public MissingMandatoryParameterException(String code, String msg) {
        super(code, msg);
    }

    public MissingMandatoryParameterException(ResponseSubCodeEnum subCodeEnum) {
        super(subCodeEnum != null ? subCodeEnum.getCode() : null, subCodeEnum != null ? subCodeEnum.getMsg() : null);
    }
}
