package com.zcj.common.web.exception;

import com.zcj.common.web.constant.ResponseSubCodeEnum;

/**
 * @classname InvalidParameterException
 * @description 非法的参数（顶级code为：40002）
 * @author: zchun
 * @version: 1.0
 * @date: 2021/7/4 13:24
 */
public class InvalidParameterException extends ResponseException {

    public InvalidParameterException(String code, String msg) {
        super(code, msg);
    }

    public InvalidParameterException(ResponseSubCodeEnum subCodeEnum) {
        super(subCodeEnum != null ? subCodeEnum.getCode() : null, subCodeEnum != null ? subCodeEnum.getMsg() : null);
    }
}
