package com.zcj.common.web.exception;

import com.zcj.common.web.constant.ResponseSubCodeEnum;

/**
 * @classname InsufficientAuthorityException
 * @description 权限不足异常（顶级code为：20001）
 * @author: zchun
 * @version: 1.0
 * @date: 2021/7/4 13:26
 */
public class InsufficientAuthorityException extends ResponseException {

    public InsufficientAuthorityException(String code, String msg) {
        super(code, msg);
    }

    public InsufficientAuthorityException(ResponseSubCodeEnum subCodeEnum) {
        super(subCodeEnum != null ? subCodeEnum.getCode() : null, subCodeEnum != null ? subCodeEnum.getMsg() : null);
    }
}
