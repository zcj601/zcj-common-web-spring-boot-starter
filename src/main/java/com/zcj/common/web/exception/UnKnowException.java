package com.zcj.common.web.exception;

/**
 * @classname UnKnowException
 * @description 未知异常，一般为404，500等错误
 * @author: zchun
 * @version: 1.0
 * @date: 2021/7/4 17:06
 */
public class UnKnowException extends ResponseException {

    public UnKnowException(String code, String msg) {
        super(code, msg);
    }

}
