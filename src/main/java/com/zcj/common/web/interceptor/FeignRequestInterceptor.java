package com.zcj.common.web.interceptor;

import com.zcj.common.web.constant.CloudConsts;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Base64Utils;

/**
 * @classname FeignRequestInterceptor
 * @description feign 请求设置头部
 * @author: zchun
 * @version: 1.0
 * @date: 2021/9/2 22:42
 */
@Configuration
public class FeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        template.header(CloudConsts.GATEWAY_TOKEN_HEADER, new String(Base64Utils.encode((CloudConsts.GATEWAY_TOKEN_VALUE).getBytes())));
    }
}
