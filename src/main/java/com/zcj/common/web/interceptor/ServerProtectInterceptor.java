package com.zcj.common.web.interceptor;

import com.zcj.common.web.constant.CloudConsts;
import com.zcj.common.web.constant.ResponseCodeEnum;
import com.zcj.common.web.constant.ResponseSubCodeEnum;
import com.zcj.common.web.dto.ResponseDTO;
import com.zcj.common.web.properties.CloudSecurityProperties;
import com.zcj.common.web.util.WebUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Base64Utils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @classname ServerProtectInterceptor
 * @description 拦截器
 * @author: zchun
 * @version: 1.0
 * @date: 2021/8/29 16:39
 */
public class ServerProtectInterceptor implements HandlerInterceptor {

    private CloudSecurityProperties properties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!properties.getOnlyFetchByGateway()) {
            return true;
        }

        // 获取头部值
        String token = request.getHeader(CloudConsts.GATEWAY_TOKEN_HEADER);

        // 获取正确的值
        String gatewayToken = new String(Base64Utils.encode(CloudConsts.GATEWAY_TOKEN_VALUE.getBytes()));

        // 验证请求
        if (StringUtils.equals(gatewayToken, token)) {
            return true;
        } else {
            ResponseDTO dto = new ResponseDTO();
            dto.setCode(ResponseCodeEnum.SERVICE_UNAVAILABLE.getCode());
            dto.setMsg(ResponseCodeEnum.SERVICE_UNAVAILABLE.getMsg());
            dto.setSubCode(ResponseSubCodeEnum.AOP_UNKNOW_ERROR.getCode());
            dto.setSubMsg("请通过网关访问资源");
            WebUtils.writeJson(response, dto);
            return false;
        }
    }

    public void setProperties(CloudSecurityProperties properties) {
        this.properties = properties;
    }
}
